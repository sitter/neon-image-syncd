# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>

clean:
	rm -rf doc

node_modules:
	npm install apidoc

doc:
	node_modules/.bin/apidoc \
		--debug \
		-e node_modules \
		-e vendor \
		-e doc \
		-o doc

test:
	go test -v ./...

.PHONY: doc deploy
